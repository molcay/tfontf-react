import React from 'react';
import PropTypes from 'prop-types';


const SearchedUser = ({ user }) => {
  return (
    <div className="columns is-centered">
      <div className="column has-text-centered is-5">
        <div className="box has-background-light">
          <div className="media">
            <div className="media-left">
              <figure className="image is-48x48">
                <img className="is-rounded" src={user.avatar_url} alt={`${user.login} avatar`} />
              </figure>
            </div>
            <div className="media-content">
              <p className="title is-4">{user.name}</p>
              <p className="subtitle is-6"><a className="header" href={user.html_url} target="_blank" rel="noopener noreferrer">@{user.login}</a></p>
              <p>{user.location}</p>
              <p>{user.bio}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

SearchedUser.propTypes = {
  user: PropTypes.object.isRequired,
}

const SingleUser = ({ user }) => {
  return (
    <div className="column is-one-quarter">
      <a href={user.html_url} target="_blank" rel="noopener noreferrer">
        <div className="box">
          <div className="media">
            <div className="media-left">
              <figure className="image is-48x48">
                <img className="is-rounded" src={user.avatar_url} alt={`${user.login} avatar`} />
              </figure>
            </div>
            <div className="media-content">
              <p>{user.login}</p>
            </div>
          </div>
        </div>
      </a>
    </div>
  )
}

SingleUser.propTypes = {
  user: PropTypes.object.isRequired,
}

const User = ({ user, isInUserList }) => {
  const Component = isInUserList ? SingleUser : SearchedUser;

  return <Component user={user} />
};

User.propTypes = {
  user: PropTypes.object.isRequired,
  isInUserList: PropTypes.bool,
}

export default User;
