import React from 'react';
import PropTypes from 'prop-types';
import User from '../user/User.js';


const UserList = ({ listToShow }) => {
  const listView = listToShow.map(user => <User user={user} key={user.id} isInUserList />)

  return (
    <div className="section">
      <div className="columns is-multiline">
        {listView}
      </div>
    </div >
  )
}

UserList.propTypes = {
  listToShow: PropTypes.array.isRequired,
}

export default UserList;
