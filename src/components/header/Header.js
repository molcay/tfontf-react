import React from 'react';
import logo from '../../assets/images/GitHub_Logo.png';
import './Header.css';


const Header = () => {
  return (
    <header className="columns is-centered">
      <div className="column has-text-centered">
        <img src={logo} className="App-logo" alt="logo" />
      </div>
    </header>
  )
}

export default Header;
