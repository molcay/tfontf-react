import React from 'react';
import { connect } from "react-redux";
import API from '../../utils/api';

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.getAll = this.getAll.bind(this);
  }

  getAll() {
    // TODO: implement this => api.get  
    const username = this.props.searchTerm;
    this.props.clearSearched();
    this.props.showLoading();
    Promise.all([API.getUserInfo(username), API.getFollowers(username), API.getFollowings(username)]).then(([userInfoResponse, followersResponse, followingsResponse]) => {
      this.props.setUserInfo(userInfoResponse);
      this.props.setFollowers(followersResponse);
      this.props.setFollowings(followingsResponse);
      this.props.setFansAndNotFollowers();
    }).then(() => {
      this.props.hideLoading();
      this.props.setSearched();
    })

  }

  _handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.getAll()
    }
  }

  handleOnChange = (e) => {
    const text = e.target.value;
    this.props.setSearchTerm(text);
  }

  render() {
    return (
      <div className="columns is-marginless is-centered">
        <div className="column has-text-centered">
          <div className="field has-addons">
            <p className="control is-normal is-expanded">
              <input
                className="input is-normal is-rounded"
                placeholder="Enter a username"
                type="text"
                onChange={this.handleOnChange}
                onKeyPress={this._handleKeyPress}
              />
            </p>
            <p className="control">
              <button
                className="button is-info is-normal is-rounded"
                onClick={this.getAll}
                disabled={!!!this.props.searchTerm}
              >
                <i className="fa fa-search"></i>
              </button>
            </p>
          </div>
        </div>
      </div >
    )
  }
};

const mapStateToProps = (state /*, ownProps*/) => {
  return {
    searchTerm: state.main.searchTerm,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    setSearchTerm: (text) => {
      dispatch({
        type: 'SET_SEARCH_TERM',
        term: text,
      })
    },
    setSearched: () => {
      dispatch({
        type: 'SET_SEARCHED',
      })
    },
    clearSearched: () => {
      dispatch({
        type: 'CLEAR_SEARCHED',
      })
    },
    setUserInfo: (data) => {
      dispatch({
        type: 'SET_USER_INFO',
        user: data
      })
    },
    showLoading: () => {
      dispatch({
        type: 'SHOW_LOADING',
      })
    },
    hideLoading: () => {
      dispatch({
        type: 'HIDE_LOADING'
      })
    },
    setFollowers: (data) => {
      dispatch({
        type: 'SET_FOLLOWERS',
        data: data,
      })
    },
    setFollowings: (data) => {
      dispatch({
        type: 'SET_FOLLOWINGS',
        data: data,
      })
    },
    setFansAndNotFollowers: (data) => {
      dispatch({
        type: 'SET_FANS_AND_NOT_FOLLOWERS',
      })
    },
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
