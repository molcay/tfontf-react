import React from 'react';
import './Spinner.css';

const Spinner = () => {
  return (
    <div className="columns is-marginless is-centered">
      <div className="lds-ripple"><div></div><div></div></div>
    </div>
  )
}

export default Spinner;
