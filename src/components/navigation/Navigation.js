import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Navigation = ({ followers, following, fans, notFollowers }) => {
  return (
    <section className="section">
      <h3 className="subtitle">Görmek isteğiniz listeyi seçiniz</h3>
      <nav className="level">
        <div className="level-left has-centered-text">
          <Link className="button" to="/followers">Takipçiler ({followers.length})</Link>
          <Link className="button" to="/followings">Takip Edilenler ({following.length})</Link>
          <Link className="button" to="/fans">Hayranlar ({fans.length})</Link>
          <Link className="button" to="/notFollowers">Takip Etmeyenler ({notFollowers.length})</Link>
        </div>
      </nav>
    </section>
  )
};

Navigation.propTpyes = {
  followers: PropTypes.array.isRequired,
  following: PropTypes.array.isRequired,
  fans: PropTypes.array.isRequired,
  notFollowers: PropTypes.array.isRequired,
}

export default Navigation;
