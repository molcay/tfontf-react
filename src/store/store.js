import { createStore, combineReducers, applyMiddleware } from "redux";
import logger from 'redux-logger';
import mainReducer from './reducer';

const reducers = {
  main: mainReducer
};

const reducer = combineReducers(reducers);

const baseMiddlewares = [];

let middlewares;
if (process.env.NODE_ENV !== 'production') {
  middlewares = baseMiddlewares.concat(logger);
} else {
  middlewares = baseMiddlewares;
}

const store = createStore(
  reducer,
  applyMiddleware(...middlewares),
  window.config,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() // eslint-disable-line no-underscore-dangle
);

export default store;
