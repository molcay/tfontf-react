import actions from "./actions";

const reducer = (state = { listToShow: [] }, action) => {
  const actor = actions[action.type];

  if (!actor) {
    return state;
  }

  return actor(state, action);
};

export default reducer;
