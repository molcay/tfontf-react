import {
  sortByLogin,
  diffListById,
} from '../utils/helpers';

const setState = (state, data) => Object.assign({}, state, data)

const actions = {
  SET_USER_INFO: (state, action) => {
    return setState(state, { searchedUser: action.user })
  },
  SET_FOLLOWERS: (state, action) => {
    const followers = sortByLogin(action.data);
    return setState(state, { followers: followers })
  },
  SET_FOLLOWINGS: (state, action) => {
    const followings = sortByLogin(action.data);
    return setState(state, { followings: followings })
  },
  SET_FANS_AND_NOT_FOLLOWERS: (state) => {
    const followers = state.followers;
    const followings = state.followings;
    const fans = diffListById(followers, followings);
    const notFollowers = diffListById(followings, followers);
    return setState(state, {
      fans: sortByLogin(fans),
      notFollowers: sortByLogin(notFollowers),
    })
  },
  SET_SEARCH_TERM: (state, action) => {
    return setState(state, { searchTerm: action.term })
  },
  SET_SEARCHED: (state) => {
    return setState(state, { searched: true })
  },
  CLEAR_SEARCHED: (state) => {
    return setState(state, { searched: false })
  },
  SHOW_LOADING: (state) => {
    return setState(state, { isLoading: true })
  },
  HIDE_LOADING: (state) => {
    return setState(state, { isLoading: false })
  }
};

export default actions;
