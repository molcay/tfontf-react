export const sortByLogin = (list) => {
  return list.sort((u1, u2) => u1.login.localeCompare(u2.login))
};

export const diffListById = (list1, list2) => {
  const list2IdList = list2.map((u2) => u2.id);
  return list1.filter((u) => list2IdList.indexOf(u.id) === -1);
};
