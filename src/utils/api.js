const Github = require('gh.js');

const gh = new Github();

class API {
  _get(username, endpoint) { // TODO: implement this with tail recursive
    let URL_parts = ['users', username]

    if (endpoint) {
      URL_parts.push(endpoint)
    }

    const URL = URL_parts.join('/')

    return new Promise((resolve, reject) => {
      gh.get(URL, { all: true }, (err, user) => {
        if (err) reject(err)
        resolve(user)
      })
    })
  }

  getUserInfo(username) {
    return this._get(username).then(u => u[0])
  }

  getFollowers(username) {
    return this._get(username, 'followers')
  }

  getFollowings(username) {
    return this._get(username, 'following')
  }
}

export default new API();
