import React, { Component } from 'react';
import { Route, Switch, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import Header from './components/header/Header.js';
import Search from './components/search/Search.js';
import Navigation from './components/navigation/Navigation.js';
import UserList from './components/user-list/UserList.js';
import User from './components/user/User.js';
import 'bulma/css/bulma.css'
import './App.css';
import Spinner from './components/spinner/Spinner.js';


class App extends Component {
  state = {
    followers: [],
    followings: [],
    fans: [],
    notFollowers: [],
    user: {},
  }

  render() {
    const {
      user,
      followers,
      followings,
      fans,
      notFollowers,
      searched,
      isLoading,
    } = this.props;

    return (
      <div className="App container">
        <Header />
        <Search />
        {searched && (
          <React.Fragment>
            <User user={user} />
            <Navigation
              followers={followers}
              following={followings}
              fans={fans}
              notFollowers={notFollowers}
            />
            <Switch>
              <Route exact path="/notFollowers" render={props => <UserList {...props} listToShow={notFollowers} />} />
              <Route exact path="/fans" render={props => <UserList {...props} listToShow={fans} />} />
              <Route exact path="/followings" render={props => <UserList {...props} listToShow={followings} />} />
              <Route path="/*" render={props => <UserList {...props} listToShow={followers} />} />
            </Switch>
          </React.Fragment>
        )}
        {isLoading && (<Spinner />)}
      </div>
    );
  }
}

const mapStateToProps = (state /*, ownProps*/) => {
  return {
    searchTerm: state.main.searchTerm,
    searched: state.main.searched,
    followers: state.main.followers || [],
    followings: state.main.followings || [],
    fans: state.main.fans || [],
    notFollowers: state.main.notFollowers || [],
    user: state.main.searchedUser || {},
    isLoading: state.main.isLoading || false,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
